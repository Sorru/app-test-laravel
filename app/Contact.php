<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Contact extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contacts';

	/**
	 * The attributes that are instances of Carbon
	 * @var string
	 */
	protected $dates = ['data_nasterii'];

	/**
	 * Activate timestamps
	 */
	public $timestamps = true;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nume', 
		'prenume', 
		'email', 
		'mobil', 
		'sex', 
		'data_nasterii', 
		'cnp'
	];

	/**
	 * Format data_nasterii to mysql format before insertion
	 */
	public function setDataNasteriiAttribute($value)
	{
		$this->attributes['data_nasterii'] = Carbon::parse($value)->format('Y-m-d');
	}

	/**
	 * Format data_nasterii to romanian format after extraction
	 */
	public function getDataNasteriiAttribute($value)
	{
		return Carbon::parse($value)->format('d.m.Y');
	}

}