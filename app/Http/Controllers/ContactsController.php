<?php namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;
use Illuminate\HttpResponse;
use Session;
// use Illuminate\Http\Request;
// use Request;

class ContactsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$contacts = Contact::latest()->get();

		return view('contacts.index')->with('contacts', $contacts);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('contacts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ContactRequest $request)
	{
		$contact = Contact::create($request->all());

		Session::flash('message', 'Contactul a fost creat cu succes!'); 
		Session::flash('alert-class', 'alert-success'); 

		return redirect('/contacts/'.$contact->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{	
		$contact = Contact::findOrFail($id);

		return view('contacts.show')->with('contact', $contact);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$contact = Contact::findOrFail($id);

		return view('contacts.edit')->with('contact', $contact);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, ContactRequest $request)
	{
		$contact = Contact::findOrFail($id);

		if ($contact->update($request->all())) {

			Session::flash('message', 'Contactul a fost actualizat cu succes!'); 
			Session::flash('alert-class', 'alert-info'); 

			return redirect('contacts/'.$contact->id);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$contact = Contact::findOrFail($id);

		if ($contact->delete()) {
			Session::flash('message', 'Contactul a fost sters cu succes!'); 
			Session::flash('alert-class', 'alert-danger'); 
		} else {
			Session::flash('message', 'Contactul nu a putut fi sters!'); 
			Session::flash('alert-class', 'alert-warning'); 
		}

		return redirect('/');
	}

}
