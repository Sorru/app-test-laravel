<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		$rules = [
			'nume' => 'required|max:60',
			'prenume' => 'required|max:60',
			'mobil' => 'required|min:10|max:12|alpha_dash',
			'email' => 'unique:contacts|required|email',
			'data_nasterii' => 'required|size:10|date|date_format:d.m.Y',
			'sex' => 'required|in:masculin,feminin',
			'cnp' => 'required|size:13'
		];

		//email rule override for updates
		if (Request::isMethod('PATCH'))
		{
		    $rules['email'] = 'unique:contacts,email,'.Request::get('id').'|required|email';
		}

		return $rules;
	}

	/**
	 * Custom Validation Messages
	 */
	public function messages()
	{
		return [
			'required' => 'Va rugam sa introduceti o valoare pentru :attribute .',
			'email' => ':attribute nu este  un email valid.',
			'unique' => 'Emailul este deja folosit, va rugam sa introduceti un altul.',
			'max' => ':attribute trebuie sa fie maximum :max caractere',
			'min' => ':attribute trbuie sa fie cel putin :min caractere',
			'size' => ':attribute trebuie sa fie exact :size caractere',
			'date' => ':attribute trebuie sa fie de tip data.',
			'in' => ':attribute trebuie sa fie unul dintre urmatoarele :values',
			'date_format' => ':attribute nu este in formatul correct :format'
		];
	}

}