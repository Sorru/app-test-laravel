var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */


// elixir(function(mix) {
//     mix.styles([
//         "app.css"
//     ], 'public/css/app.css', 'resources/css');
// });

// elixir(function(mix) {
//     mix.styles([
//         "app.css",
//         "app.css"
//     ], 'public/css/everything.css');
// });

elixir(function(mix) {
    mix.copy('public/css', 'resources/css');
});