<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Manager Contacte</title>

	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datepicker.css')}}">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}">

	<script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('js/bootstrap-datepicker.ro.min.js')}}"></script>
	<script src="{{asset('js/jquery.validate.js')}}"></script>
	<script src="{{asset('js/additional-methods.js')}}"></script>
	<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_colapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{url('/')}}">Manager Contacte</a>
			</div>

			<div class="collapse navbar-collapse" id="navbar_colapse">
				<ul class="nav navbar-nav">
					<li class="{{ (Request::is('/') ? 'active' : '') }}"><a href="{{url('/')}}"><i class="fa fa-search"> </i> Vizualizare Contacte</a></li>
					<li class="{{ (Request::is('contacts/create') ? 'active' : '') }}"><a href="{{url('/contacts/create')}}"><i class="fa fa-pencil"> </i> Adauga Contacte</a></li>
				</ul>
			</div>

		</div>
	</nav>
	<div class="container">
		@yield('content')
	</div>

	@yield('scripts')
</body>
</html>