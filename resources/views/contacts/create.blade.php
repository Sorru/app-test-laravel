@extends('layout.main')
@section('content')
	<h1>Adauga contact</h1>
	<hr>
	{!! Form::open(array('action' => 'ContactsController@store', 'class' => 'form-horizontal', 'id' => 'form')) !!}

	@include('contacts.partials.form')

	<div class="form-group">
		{!! Form::submit('Adaugat Contact', array('class' => 'btn btn-info form-control') ) !!}
	</div>	
		
	{!! Form::close() !!}
@stop
	@section('scripts')
	@include('contacts.partials.scripts')
	@include('contacts.partials.modal')
@stop