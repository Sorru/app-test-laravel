@extends('layout.main')
@section('content')
	<h1>Actualizeaza contact</h1>
	<hr>
	{!! Form::model($contact, array('action' => array('ContactsController@update', $contact->id), 'id' => 'form', 'method' => 'PATCH','class' => 'form-horizontal')) !!}
	{!! Form::hidden('id', null)!!}
	@include('contacts.partials.form')
		
	<div class="form-group">
		{!! Form::submit('Actualizeaza Contact', array('class' => 'btn btn-info form-control') ) !!}
	</div>	

	{!! Form::close() !!}
@stop
@section('scripts')
	@include('contacts.partials.scripts')
	@include('contacts.partials.modal')
@stop