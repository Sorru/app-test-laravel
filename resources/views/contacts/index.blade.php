@extends('layout.main')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h2>Contacte</h2>
	</div>
</div>
<hr>
@if(Session::has('message'))
<div class="row">
	<div class="col-sm-7 col-sm-offset-2 alert {{ Session::get('alert-class', 'alert-info') }}">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		{{Session::get('message')}}
	</div>
</div>
@endif
<div class="row">
	<div class="col-sm-12">
		<table class="table" id="contacts">
			<thead>
				<tr>
					<th>Nr.</th>
					<th>Nume</th>
					<th>Prenume</th>
					<th>Mobil</th>
					<th>Email</th>
					<th>Data Nasterii</th>
					<th>Sex</th>
					<th>CNP</th>
					<th>Vezi</th>
					<th>Editeaza</th>
					<th>Sterge</th>
				</tr>
			</thead>
			<tbody>
			@foreach($contacts as $contact)
				<tr>
					<td>{{$contact->id}}</td>
					<td>{{$contact->nume}}</td>
					<td>{{$contact->prenume}}</td>
					<td>{{$contact->mobil}}</td>
					<td>{{$contact->email}}</td>
					<td>{{$contact->data_nasterii}}</td>
					<td>{{ucfirst($contact->sex)}}</td>
					<td>{{$contact->cnp}}</td>
					<td>
						<a href="{{url('contacts/'.$contact->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-search"> </i>  Vizualizeaza Contact</a>
					</td>
					<td>
						<a href="{{url('contacts/'.$contact->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Actualizeaza Contact</a>
					</td>
					<td>
						{!! Form::open(array('action' => array('ContactsController@destroy', $contact->id), 'method' => 'delete')) !!}
					        <button type="submit" class="btn btn-warning btn-sm modal-delete"><i class="fa fa-ban"></i> Sterge Contact</button>
					    {!! Form::close() !!}
					</td>
				</tr>
		@endforeach
			</tbody>
		</table>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6 col-sm-offset-2">
		<a href="{{url('contacts/create')}}" class="btn btn-success"><i class="fa fa-user-plus"> </i> Adauga Contact</a>
	</div>
</div>
@stop
@section('scripts')

@include('contacts.partials.modal')

<script>
$(document).ready(function() {
	$('#contacts').dataTable({
		"language" : {
			"url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Romanian.json"
		},
	});
	$('#contacts').removeClass( 'display' ).addClass('table ');

	$(".alert").fadeTo(4400, 500).slideUp(500, function(){
		$(".alert").alert('close');
	});

} );
</script>
@stop