<div id="delete_modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmare</h4>
			</div>
			<div class="modal-body">
				<p>Confirmati stergerea contactului?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Inchide</button>
				<button type="button" class="btn btn-primary" id="delete_confirm"><i class="fa fa-check"></i> Confirm</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(".modal-delete").click(function(event) {

		event.preventDefault();
		
		var form = $(this).parents('form:first');

		console.log(form);

		$("#delete_modal").modal('show');

		$("#delete_confirm").click(function() {
			form.submit();
		});
	});
</script>