<div class="form-group">
	{!! Form::label('nume', 'Nume', array('class' => 'control-label col-sm-3') )!!}
	<div class="col-sm-7">
		{!! Form::text('nume', null, array('class' => 'form-control'))!!}
		@if($errors->first('nume'))
		<div class="error">
			{{$errors->first('nume')}}
		</div>
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('prenume', 'Prenume', array('class' => 'control-label col-sm-3') )!!}
	<div class="col-sm-7">
		{!! Form::text('prenume', null, array('class' => 'form-control'))!!}
		@if($errors->first('prenume'))
		<div class="error">
			{{$errors->first('prenume')}}
		</div>
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('mobil', 'Telefon Mobil', array('class' => 'control-label col-sm-3') )!!}
	<div class="col-sm-7">
		{!! Form::text('mobil', null, array('class' => 'form-control'))!!}
		@if($errors->first('mobil'))
		<div class="error">
			{{$errors->first('mobil')}}
		</div>
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('email', 'Email', array('class' => 'control-label col-sm-3') )!!}
	<div class="col-sm-7">
		{!! Form::email('email', null, array('class' => 'form-control'))!!}
		@if($errors->first('email'))
		<div class="error">
			{{$errors->first('email')}}
		</div>
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('data_nasterii', 'Data Nasterii', array('class' => 'control-label col-sm-3') )!!}
	<div class="col-sm-7">
		{!! Form::text('data_nasterii', null, array('class' => 'form-control'))!!}
		@if($errors->first('data_nasterii'))
		<div class="error">
			{{$errors->first('data_nasterii')}}
		</div>
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('sex', 'Sex', array('class' => 'control-label col-sm-3') )!!}
	<div class="col-sm-7">
		{!! Form::select('sex', array('masculin' => 'Masculin', 'feminin' => 'Feminin'), 'masculin', array('class' => 'form-control')) !!}
		@if($errors->first('sex'))
		<div class="error">
			{{$errors->first('sex')}}
		</div>
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('cnp', 'Cod Numeric Personal', array('class' => 'control-label col-sm-3') )!!}
	<div class="col-sm-7">
		{!! Form::text('cnp', null, array('class' => 'form-control'))!!}
		@if($errors->first('cnp'))
		<div class="error">
			{{$errors->first('cnp')}}
		</div>
		@endif
	</div>
</div>