<script>
	$(document).ready(function() {
		
		$('#data_nasterii').datepicker({
			format: "dd.mm.yyyy",
			startDate: "01/01/1900",
			startView: 2,
			todayBtn: true,
			language: "ro",
			endDate: '+0d',
			autoclose: true
		});

		$("#form").validate({
			rules: {
				email: {
					required: true,
					email: true
				},
				nume: {
					required: true,
					minlength: 2
				},
				prenume: {
					required: true,
					minlength: 2
				},
				mobil: {
					required: true,
					number: true,
					minlength: 10,
					maxlength: 12
				},
				data_nasterii: {
					required: true,
					rangelength: [10, 10]
				},
				sex: {
					required: true
				},
				cnp: {
					required: true,
					number: true,
					rangelength: [13, 13]
				}
			},
			messages : {

				email: {
					required: "Va rugam introduceti un email.",
					email: "Va rugam introduceti un email valid!"
				},
				nume: {
					required: "Va rugam sa introduceti un nume.",
					minlength: "Numele este prea scurt"
				},
				prenume: {
					required: "Va rugam sa introduceti un prenume.",
					minlength: "Prenumele este prea scurt"
				},
				mobil: {
					required: "Va rugam sa introduceti un numar de telefon",
					number: "Va rugam sa introduceti doar cifre",
					minlength: "Numarul de telefon nu poate avea mai putin de 10 cifre",
					maxlength: "Numarul de telefon nu poate avea mai putin de 13 cifre"
				},
				data_nasterii: {
					required: "Va rugam sa introduceti data nasterii",
					rangelength: "Data nasterii trebuie sa aiba exact 10 caractere"
				},
				sex: {
					required: "Va rugam sa alegeti sexul contactului"
				},
				cnp: {
					required: "Va rugam sa completati campul Cod Numeric Personal",
					number: "Va rugam sa introduceti doar cifre",
					rangelength: "CNP-ul trebuie sa abia exact 13 cifre."
				}
			}
		});
	});




</script>