@extends('layout.main')

@section('content')

<div class="row">
	<div class="col-sm-12">
		<h1>Vizualizare Contact - {{$contact->nume}} {{$contact->prenume}}</h1>
	</div>
</div>

@if(Session::has('message'))
<div class="row">
	<div class="col-sm-7 col-sm-offset-2 alert {{ Session::get('alert-class', 'alert-info') }}">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		{{Session::get('message')}}
	</div>
</div>
@endif
<hr>
<div class="row">
	<div class="col-sm-6">
		<strong>Nume</strong>
	</div>
	<div class="col-sm-6">
		{{$contact->nume}}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<strong>Prenume</strong>
	</div>
	<div class="col-sm-6">
		{{$contact->prenume}}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<strong>Telefon Mobil</strong>
	</div>
	<div class="col-sm-6">
		{{$contact->mobil}}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<strong>Email</strong>
	</div>
	<div class="col-sm-6">
		{{$contact->email}}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<strong>Data Nasterii</strong>
	</div>
	<div class="col-sm-6">
		{{$contact->data_nasterii}}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<strong>Cod numeric personal</strong>
	</div>
	<div class="col-sm-6">
		{{$contact->cnp}}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-6">
		<strong>Sex</strong>
	</div>
	<div class="col-sm-6">
		{{ucfirst($contact->sex)}}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-3 col-sm-offset-2">
		<a href="{{url('/')}}" class="btn btn-success"><i class="fa fa-search"> </i> Vezi Toate Contactele</a>
	</div>
	<div class="col-sm-3">
		<a href="{{url('contacts/'.$contact->id.'/edit/')}}" class="btn btn-info"><i class="fa fa-pencil"></i> Actualizeaza Contact</a>
	</div>
	<div class="col-sm-3">
		{!! Form::open(array('action' => array('ContactsController@destroy', $contact->id), 'method' => 'delete')) !!}
	        <button type="submit" class="btn btn-warning modal-delete"><i class="fa fa-ban"></i> Sterge Contact</button>
	    {!! Form::close() !!}
	</div>
</div>

@stop