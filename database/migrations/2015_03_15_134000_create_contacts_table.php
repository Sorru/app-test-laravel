<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nume', 60);
			$table->string('prenume', 60);
			$table->string('mobil', 12);
			$table->string('email', 60)->unique();
			$table->date('data_nasterii');
			$table->enum('sex', ['masculin', 'feminin']);
			$table->string('cnp', 13);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
